# Trainning DumbWays.id
Complete List of my Trainning at DumbWays.id

## Week 1
- [Setup Ubuntu Server 18.04 at VMWare](https://gitlab.com/ledleledle/install-ubuntu-server-18.04-vmware)
- [Setup Server at AWS Cloud & Deploy Node.JS Application](https://gitlab.com/ledleledle/praktek-aws)

## Week 2
- [Git, SSH Keys, Setup Database & Backend Server](https://gitlab.com/ledleledle/week-2-bootcamp-dw)

## Week 3
- [Docker, Setup NAT Network, Jenkins](https://gitlab.com/ledleledle/week-3-bootcamp-dumbways)

## Week 4
- [Ansible, Server Monitoring, cPanel](https://gitlab.com/ledleledle/week-4-dumbways)

## Final Exam
- [Final Exam](https://gitlab.com/ledleledle/final-exam-dw)